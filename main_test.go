package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

func Router() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/", homePageHandler)
	router.HandleFunc("/viewdir/", viewDirHandler).Methods("GET")
	return router
}

func TestHomePageHandler(t *testing.T) {
	// test home page
	request, _ := http.NewRequest("GET", "/", nil)
	reponse := httptest.NewRecorder()
	Router().ServeHTTP(reponse, request)
	assert.Equal(t, 200, reponse.Code, "OK reponse is expected")
}

func TestViewDirHandler(t *testing.T) {
	// test JSON with subdir
	request, _ := http.NewRequest("GET", "/viewdir/?output=JSON&path=.&inclSubDir=inclSubDir", nil)
	reponse := httptest.NewRecorder()
	Router().ServeHTTP(reponse, request)
	assert.Equal(t, 200, reponse.Code, "OK reponse is expected")
	//fmt.Println(reponse.Body)

	// test HTML without subir
	request, _ = http.NewRequest("GET", "/viewdir/?output=HTML&path=.", nil)
	reponse = httptest.NewRecorder()
	Router().ServeHTTP(reponse, request)
	assert.Equal(t, 200, reponse.Code, "OK reponse is expected")
	//fmt.Println(reponse.Body)
}
