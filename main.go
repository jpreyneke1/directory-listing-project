package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"github.com/gorilla/mux"
)

type Dir struct {
	Path         string `json:"Path"`
	Size         string `json:"Size"`
	Permissions  string `json:"Permissions"`
	ModifiedDate string `json:"ModifiedDate"`
}

// global variables
var dirList []Dir

func main() {

	log.Println("<> Started : Directory Listing WebApp <>")

	//initialise router
	router := mux.NewRouter()

	//endpoints
	router.HandleFunc("/", homePageHandler)
	router.HandleFunc("/viewdir/", viewDirHandler).Methods("GET")

	//serve
	log.Fatal(http.ListenAndServe(":8080", router))

}

func homePageHandler(w http.ResponseWriter, req *http.Request) {
	log.Println("** Endpoint: homePageHandler")

	// create input form
	fmt.Fprintf(w,
		"<style>body {background-color: powderblue; font-size: 10px;} h1 {color: blue;} input {font-size: 12px;} </style>"+
			"<body>"+
			"<h1>Directory Listing WebApp</h1>"+
			"<form action=\"/viewdir/\" method=\"GET\">"+
			"<label>Output:</label><br>"+
			"<input type=\"radio\" id=\"html\" name=\"output\" value=\"HTML\" checked=\"checked\">"+
			"<label for=\"html\">HTML</label><br>"+
			"<input type=\"radio\" id=\"json\" name=\"output\" value=\"JSON\">"+
			"<label for=\"json\">JSON</label><br><br>"+
			"<label for=\"path\">Directory Path (blank = root, . = current dir): </label><br>"+
			"<input type=\"text\" id=\"path\" name=\"path\"><br><br>"+
			"<input type=\"checkbox\" id=\"inclSubDir\" name=\"inclSubDir\" value=\"inclSubDir\" checked>"+
			"<label for=\"inclSubDir\">Include subdirectories</label><br><br>"+
			"<input type=\"submit\" value=\"View\">"+
			"</form>"+
			"</body>")
}

func viewDirHandler(w http.ResponseWriter, req *http.Request) {
	log.Println("** Endpoint: viewDirHandler")

	// retrieve arguments
	args := req.URL.Query()
	log.Println("-- Arguments received: ", args)

	// assign variables / default
	dirList = nil
	var output string
	var path string = "/"
	var inclSubDir bool = true

	// if inclSubDir was unassigned
	if len(args["inclSubDir"]) == 0 {
		inclSubDir = false
	}
	// blank = root
	if args["path"][0] != "" {
		path = args["path"][0]
	}
	// set output
	output = args["output"][0]
	if output == "JSON" {
		w.Header().Set("Content-Type", "application/json")
	} else { // default HTML
		fmt.Fprintf(w,
			"<style>body {background-color: powderblue; font-size: 10px;} h1 {color: blue;} td {font-size: 10px; border: 1px solid;} th {font-size: 10px; border: 1px solid; font-weight: bold;} table { border-collapse: collapse;} </style>"+
				"<body>"+
				"<h1>Directory Listing WebApp Results</h1>"+
				"<table>"+
				"<tr>"+
				"<th>Path</th>"+
				"<th>Size(Bytes)</th>"+
				"<th>Permissions</th>"+
				"<th>Last Modified</th>"+
				"<th>IsDir</th>"+
				"</tr>")
	}

	// check if path exists ( . represents execution dir)
	if path != "." {
		_, err := os.Stat(path)
		if os.IsNotExist(err) {
			// page 404 + empty json
			log.Println("!! Path/File does not exist")
			w.WriteHeader(http.StatusNotFound)
			w.Header().Set("Content-Type", "application/json")

			resp := make(map[string]string)
			resp["message"] = "Resource Not Found"
			jsonResp, err := json.Marshal(resp)
			if err != nil {
				log.Fatalf("!! Error happened in JSON marshal. Err: %s", err)
			}
			w.Write(jsonResp)
			return
		}
	}
	log.Println("-- Valid path provided")

	// variable to prevent skipping the very first(main) directory if subdirectories are to be excluded
	firstDir := true
	// loop through directory path
	filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Println(err.Error())
			return nil
		}
		if info.IsDir() && !inclSubDir {
			if !firstDir {
				return filepath.SkipDir
			}
			firstDir = false
		}
		if output == "JSON" {
			dirList = append(dirList,
				Dir{Path: path,
					Size:         strconv.FormatInt(info.Size(), 10),
					Permissions:  info.Mode().String(),
					ModifiedDate: info.ModTime().String()})
		} else { // default HTML
			fmt.Fprintf(w,
				"<tr><td>"+path+"</td>"+
					"<td>"+strconv.FormatInt(info.Size(), 10)+"</td>"+
					"<td>"+info.Mode().String()+"</td>"+
					"<td>"+info.ModTime().String()+"</td>"+
					"<td>"+strconv.FormatBool(info.IsDir())+"</td>"+
					"</tr>")
		}
		return nil
	})
	log.Println("-- Loop through files : Success")

	if output == "JSON" {
		json.NewEncoder(w).Encode(dirList)
	} else { // default HTML (close HTML table)
		fmt.Fprintf(w,
			"</table></body>")
	}
	log.Println("-- Client Output : Success")
}
