# Directory Listing project

## Name
Directory Listing WebApp (Using Golang & Docker)

## Description
View directories inside a Docker container:<br>
Exposes a RESTful interface on port 8080 and allows a client application to obtain the full directory listing of a given path in the container (JSON).<br>
The application also provides a homepage to view directories in HTML.

## Visuals
<img src="https://i.postimg.cc/cLvYs7gD/homepage.png" alt="homepage">


## Installation
To install you can either create a Docker image OR you can download and install Golang with the required Golang libraries.

**- To install using Docker:**<br>
1. First you will need to **download and install Docker**. Download link : https://www.docker.com/products/docker-desktop<br>
2. Please **follow** the required steps per the **Docker installation documentation** (on their website) to ensure Docker is running on your system before you continue.<br>
3. Next we will need to **create a Docker image**.
4. To create a Docker image you wil need to **navigate to the root directory of the project** and run the following command in your terminal Powershell / Command Prompt on Windows) : 
5. Run Command: **docker build -t dir-listing-proj .** _(please note that there is a "test_dir" folder which you can delete. it was used for testing purposes)_
6. To list all the images created run following command in your terminal. Run Command: **docker image ls**
7. To run the image locally, run the following command in your terminal. Run Command: **docker run --publish 8080:8080 dir-listing-proj** _(this will expose the docker image to your localhost:8080 port)_
8. To access the application open the **URL localhost:8080** in your web browser.


## Usage
Very simple to use!
You can either **view directories in HTML/JSON via the homepage** at localhost:8080 (locally run) **OR** you can **consume the API** via the the following URL and structure:<br>
<br>
The following URL will return all files/directories + subdirectories in JSON at the execution location<br>

E.g. (run locally)
localhost:8080/**viewdir**/?**output**=JSON&**path**=.&**inclSubDir**=inclSubDir

**There are 3 arguments:**

- **output** = JSON OR HTML<br>
- **path** = specified directory location<br>
- **inclSubDir** = inclSubDir to include subdirectories / to not include subdirectories just remove this argument

You can use the homepage to test the different output criteria and how it transforms the URL.

## Support
If you have any questions or need any help you can contact me at jpreyneke1@gmail.com

## Roadmap
Nothing planned at the moment.

## License
MIT
